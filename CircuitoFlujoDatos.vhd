
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx primitives in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity CircuitoFlujoDatos is
          A,B: IN std_logic;
          C: out std_logic);

end CircuitoFlujoDatos;

architecture Behavioral of CircuitoFlujoDatos is

begin
  process(A,B)-- es sensible cuando se tiene una variacion de estas se�ales a y b
  begin
    if A = 1 or B= 1 or C=1 then
     C <= '1';
    else C<='0';
   end if;
  end process;
end Behavioral;


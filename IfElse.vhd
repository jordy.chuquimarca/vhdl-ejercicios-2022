
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx primitives in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity IfElse is
 Port ( a,b : in  STD_LOGIC_VECTOR (3 downto 0);
           X,Y,Z : out  STD_LOGIC );

end IfElse;

architecture Behavioral of IfElse is

begin

process (a,b) begin
 if (a = b) then 
 x <= '1';y <= '0';z <= '0'; 
 elsif (a >  b) then
 x <= '0';y <= '1';z <= '0' ;else
 x <= '0';y <= '0'; z <= '1';
 end if;

end Behavioral;


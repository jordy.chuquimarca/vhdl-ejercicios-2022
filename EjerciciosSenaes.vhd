-----------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx primitives in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity EjerciciosSenaes is
Port (a,b,c,d : in std_logic;
f1 : out std_logic );
end EjerciciosSenaes;

architecture Behavioral of EjerciciosSenaes is
signal x: std_logic;
begin
x <= (a and b);
f1 <= x or (c xor d );

end Behavioral;


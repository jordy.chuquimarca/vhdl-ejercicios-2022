
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx primitives in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity EjerciciosConcurrente is
Port ( A : in STD_LOGIC;
B : in STD_LOGIC;
C : in STD_LOGIC;
D : in STD_LOGIC;
E : in STD_LOGIC;
F : in STD_LOGIC;
x1, x2, x3 : inout STD_LOGIC);
end EjerciciosConcurrente;

architecture Behavioral of EjerciciosConcurrente is

begin
x1 <= (A XNOR B);
x2 <= (((C AND D) OR x1) NAND ((E XOR F) AND (C AND D)));
x3 <= ((E XOR F) AND (C AND D));

end Behavioral;



----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx primitives in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity CircuitoEstructural is
      port (a,b: in bit_vector (0 to 1);
            c : out bit);
end CircuitoEstructural;

architecture Behavioral of CircuitoEstructural is
signal x: bit_vector(0 to 1);

begin
U0:  xnor2 port map (a(0),b(0), x(0));
U1:  xnor2 port map (a(1),b(1), x(1));
U2:  and2 port map (x(0),x(1), c);

end Behavioral;


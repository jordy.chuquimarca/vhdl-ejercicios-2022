----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date:    20:32:33 05/18/2022 
-- Design Name: 
-- Module Name:    CircuitoFuncional - Behavioral 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx primitives in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity CircuitoFuncional is
port (A, B : in std_logic;
F: out in std_logic );

end CircuitoFuncional;

architecture Behavioral of CircuitoFuncional is

begin
 F <= '1' when (A = '1' and B = '1') else
    '0'


end Behavioral;


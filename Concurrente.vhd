
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx primitives in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity Concurrente is
        port (a,b,c: in std_logic; f: out std_logic);
end Concurrente;

architecture Behavioral of Concurrente is

begin
f <= '1' when (a='0' and b='0' and c='0') else
'1' when (a='0' and b='1' and c='1') else
'1' when (a='1' and b='1' and c='0') else
'1' when (a='1' and b='1' and c='1')else '0';


end Behavioral;


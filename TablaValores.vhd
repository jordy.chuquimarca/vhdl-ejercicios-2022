---------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx primitives in this code.
--library UNISIM;
--use UNISIM.VComponents.all;
--------------------------------------------
--X = 'A'B'C + 'A'BC +'ABC + ABC
--Y = 'A'BC + A 'BC + AB'C
--Z = 'A'B'C + 'AB'C + 'ABC 

entity TablaValores is
port (A, B, C: in std_logic; 
 X, Y, Z:out std_logic);

end TablaValores;

architecture Behavioral of TablaValores is

begin
 X <= (not A and not B and not C) or (not A and not B and C) or (not A and B and C) or (A and B and C);
 Y <= (not A and not B and C) or (A and not B and C) or (A and B and not C);
 Z <= (not A and not B and not C) or (not A and B and not C) or (not A and B and C);


end Behavioral;


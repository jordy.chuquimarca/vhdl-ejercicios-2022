
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx primitives in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity WhenElse is
port (E0,E1,E2,E3 in std_logic; 
      SELA, SELB, SELC: in std_logic;
      F: out std_logic);
end WhenElse;

architecture Behavioral of WhenElse is

begin
F <= E3 when SELC='1' else
     E2 when SELB='1' else
     E1 when SELA='1' else
     E0;


end Behavioral;

